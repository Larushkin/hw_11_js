'use strict'

const switchPass1 = document.querySelector("#switchPass");
const switchPass2 = document.querySelector("#switchPass2");
const submitPass1 = document.querySelector("#password");
const submitPass2 = document.querySelector("#password2");


switchPass1.addEventListener("click", switchPassword);
switchPass2.addEventListener("click", switchPassword);

function validatePassword() {
    if (submitPass1.value == submitPass2.value) {
        document.querySelector("#err") ? document.querySelector("#err").remove() : null;
        alert("You are welcome");
    } else {
        document.querySelector("#err") ? document.querySelector("#err").remove() : false;
        submitPass2.insertAdjacentHTML("afterend", "<p style=\"color: red\" id=\"err\">Потрібно ввести однакові значення</p>");
    }
   
}

function switchPassword() {
    console.log(this);
    this.classList.toggle("fa-eye-slash");
    const input = this.closest("label").querySelector("input");
    const type = input.getAttribute("type") === "password" ? "text" : "password";
    input.setAttribute("type", type);
}


document.querySelector("form").addEventListener("submit", (ev) => {
    ev.preventDefault();
    validatePassword();
});
